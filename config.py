import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    CLIENT_ID = 7009239
    # REDIRECT_URI = 'http://127.0.0.1:5000'
    REDIRECT_URI = 'https://test-task-webim.herokuapp.com/'
    CLIENT_SECRET = 'RGn7gqRWpUw2tWQu2fRb'
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'A SECRET KEY'
    DEBUG = True
