from app import app
from flask import render_template, request, redirect, make_response
from vk_api import vk_api


def get_access_token(code):
    redirect_url = app.config['REDIRECT_URI']
    app_id = app.config['CLIENT_ID']
    secret = app.config['CLIENT_SECRET']

    vk_session = vk_api.VkApi(app_id=app_id, client_secret=secret)

    try:
        vk_session.code_auth(code, redirect_url)
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

    user_id = vk_session.token['user_id']
    expires_in = vk_session.token['expires_in']
    ACCESS_TOKEN = vk_session.token['access_token']
    return (ACCESS_TOKEN, user_id, expires_in)


@app.route('/', methods=['GET', 'POST'])
def index():
    args = dict(request.args)
    if 'access_token' in request.cookies.keys():
        access_token = request.cookies.get('access_token')
        vk_session = vk_api.VkApi(token=access_token)
        vk = vk_session.get_api()
        owner_id = request.cookies.get('user_id')
        user_info = vk.users.get(user_ids=owner_id, fields='photo_100', name_case='gen')[0]
        friends_list = vk.friends.get(user_id=owner_id, order='random', count=5, fields='photo_100',
                                      name_case='nom', offset=0)['items']
        context = dict(
            user_info=user_info,
            friends_list=friends_list
        )
        return render_template('home.html', **context)
    elif 'code' in args:
        url_args = get_access_token(args['code'])
        resp = make_response(redirect('/'))
        resp.set_cookie('access_token', str(url_args[0]), max_age=url_args[2])
        resp.set_cookie('user_id', str(url_args[1]), max_age=url_args[2])
        return resp
    else:
        return render_template('index.html')


@app.route('/auth')
def login():
    auth_code_flow = 'https://oauth.vk.com/authorize?client_id={0}&display=popup&redirect_uri={1}&scope=friends' \
                     '&response_type=code&v=5.110'.format(app.config['CLIENT_ID'], app.config['REDIRECT_URI'])
    return redirect(auth_code_flow)


@app.route('/logout')
def delete_cookie():
    access_token_value = request.cookies.get('access_token')
    user_id_value = request.cookies.get('user_id')
    resp = make_response(redirect('/'))
    resp.set_cookie('access_token', access_token_value, max_age=0)
    resp.set_cookie('user_id', user_id_value, max_age=0)
    return resp
